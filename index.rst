Utiliser Ubuntu
========================================================

Présentation d'Ubuntu ainsi que quelques pistes pour l'installer, la configurer
et l'utiliser en faisant gaffe à la sécurité.

.. toctree::
   :maxdepth: 2
   :caption: Table des matières :

   presentation.rst
   installation.rst
   ligne_de_commande.rst
   unix.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
