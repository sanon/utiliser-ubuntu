Installation d'Ubuntu
=====================

Choix de la version
-------------------

Ubuntu sort une nouvelle version tous les six mois : en avril et en octobre
généralement. Tous les deux ans, une version *long-term support* (*LTS*) est
publiée. Les versions normales ont des mises à jour de sécurité pendant
seulement neuf mois alors que les versions LTS en ont pendant cinq ans. Si on a
besoin des dernières nouveautés on utilisera les versions normales mais dans la
plupart des cas les versions LTS font très bien l'affaire.

Téléchargement et création de la clé USB
----------------------------------------

Le plus rapide est de télécharger l'image ISO par Bittorrent, qu'on trouve sur
`les téléchargements alternatifs d'Ubuntu
<https://ubuntu.com/download/alternative-downloads>`_. Il faut ensuite en faire
une clé USB bootable. Le plus simple dans GNOME est d'ouvrir l'application
*Disques* (``gnome-disks``), sélectionner la clé USB dans la liste des disques
puis *Restaurer l'image disque* dans le menu et sélectionner l'image ISO.

Démarrage de la clé USB
-----------------------

Quand on a la clé USB, il faut encore booter dessus. Si on avait Windows, il
vaut mieux quitter Windows en faisant redémarrer que quitter. Généralement ça va nous
aider de désactiver le mode *Secure boot* dans le BIOS. On peut parfois en
profiter pour choisir de booter sur la clé en premier. Si ce n'est pas le cas,
il va falloir trouver la combinaison de touches qui permet de booter sur la clé.
Tails est plutôt de bon conseil en la matière : `Starting Tails on PC 
<https://tails.boum.org/doc/first_steps/start/pc/index.fr.html>`_

.. note:: le BIOS c'est une sorte de petit programme installé directement dans
   le matériel de l'ordinateur, qui permet de lancer la partition ``/boot/``

Je recommande de sélectionner l'option *Essayer Ubuntu sans l'installer* pour
vérifier la compatibilité du matériel avec Ubuntu. On peut ensuite l'installer
après essai.

Le chiffrement
--------------

Pendant l'installation il est important de chiffrer l'intégralité du disque.
On peut laisser Ubuntu gérer ce qui s'appelle des partitions (une sorte de
découpage virtuel du disque, ça représente un inconvénient dans le cas des SSD
mais qui n'est pas insurmontable. La phrase de passe doit être unique et assez
longue. Elle sera demandé au démarrage du système pour déchiffrer le disque.
Sans elle impossible de lire le contenu du disque à moins :

* d'avoir accès à une faille de sécurité inconnue du chiffrement (demande
  énormément de moyens, la NSA a exploité ce genre de faille *0-day* dans
  certains contexte)
* de trouver la phrase de passe en essayant tout ce qui est possible (demande
  énormément de moyens, ça ne marche qu'à condition d'avoir une mauvaise phrase
  de passe)
* d'espionner la personne qui tape la phrase (en zieutant, en filmant et plus
  compliqué en installant un *keylogger* matériel, qui récupère les touches
  tapées) mais ça demande beaucoup de moyens
* de saisir l'ordinateur quand il est encore allumé ou qu'il vient d'être
  éteint, extraire la mémoire, la conserver dans le froid et réussir à extraire
  la phrase de passe contenue dans la mémoire (ça demande énormément de moyens là encore)
* la chose la plus probable mais qui demande des compétences techniques c'est de
  modifier la partition ``/boot`` car elle n'est pas chiffrée. En effet, le BIOS
  démarre cette partition qui contient le nécessaire pour demander à
  l'utilisateur sa phrase de passe. Ça demande quand même un accès à
  l'ordinateur.

En résumé, le chiffrement complique énormément l'accès aux données.

L'autre élément important c'est de choisir un mot de passe pour l'utilisateur.
Ce mot de passe n'a pas à être aussi compliqué que la phrase de passe mais il
est important dans la sécurité du système car il permet d'exécuter des commandes
en tant qu'administrateur (appellé aussi *root*) comme le fait d'installer des
logiciels (et donc potentiellement des logiciels malveillants).

Premier démarrage
-----------------

Ça peut être déroutant les premières fois : quand on tape son mot de passe dans
une interface de type *terminal* (donc quand on donne sa phrase de passe au
démarrage) aucun caractère ne s'affiche. C'est un choix (discutable) qui a été
fait afin de ne pas permettre à un observateur de connaître le nombre de
caractères tapés.
