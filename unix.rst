Quelques principes utiles d'Unix et de Linux
============================================

Ubuntu incorpore donc des principes d'Unix et de Linux qu'il peut être
intéressant de comprendre.

Tout est un fichier
-------------------

Il existe énormément de choses qui sont représentées par des fichiers. Le plus
important est de remarquer que c'est le cas pour les disques. Un disque a
généralement une adresse de la forme ``/dev/sda``, ``/dev/sdb``, etc. Les
partitions vont correspondre à ``/dev/sda1``, ``/dev/sda2``, etc. Donc
contrairement à Windows, on ne précise jamais la partition sur laquelle on se
place (dans Windows c'est ``C://``, ``D://``, etc.).

La conséquence c'est que le système part toujours de la racine ``/``, on y
trouve différents dossiers, c'est fréquement la même structure. À n'importe quel
niveau de cette structure on peut créer des *points de montage* qui sont une
manière de dire qu'un dossier correspond à une partition. Si on installe Ubuntu
sur un disque chiffré on a une partition ``/boot`` à part. Le dossier ``/boot``
pointe donc vers la partition. Quand on branche une clé USB, le système crée
automatiquement des points de montage dans le dossier ``/media``.

Les fichiers cachés
-------------------

Si un fichier ou un dossier commence par le caractère ``.`` on considère que
c'est un fichier caché. C'est très utilisé et on trouve facilement comment les
afficher.

Les permissions
---------------

Unix est prévu pour être un système multi-utilisateurs. On a donc des
utilisateurs, qui peuvent être rassemblés en groupes. Leurs accès aux fichiers
sont règlementés par un
système de permissions : chaque fichier a un
utilisateur et un groupe propriétaire. Pour chaque fichier on a 3 niveaux
d'accès : l'utilisateur (*user*, abrégé *u*), le groupe (*group*, *g*) et les tous les autres (*others*, *o*). On a aussi 3 types
d'accès :

* la lecture (*read*, abrégé *r*)
* l'écriture (*write*, *w*)
* l'exécution (*execute*, *x*) qui sert à pouvoir exécuter le fichier en tant
  que programme.

Dans le cas des répertoires, la lecture permet d'afficher la liste des fichiers
contenus par le répertoire, l'écriture permet d'ajouter ou supprimer des
fichiers du répertoire et l'exécution permet d'ouvrir le répertoire.

On rencontre surtout ce genre de problématique en manipulant les fichiers du
système : on peut afficher certains fichiers de configuration mais il faut être
super-utilisateur pour pouvoir les modifier.

La hiérarchie des dossiers
--------------------------

Si on regarde les dossiers dans la racine ``/`` on retrouve un certain nombre de
dossiers standards, ceux qui peuvent nous intéresser sont :

``/bin``
   contient des commandes comme ``ls``
``/boot``
   partie qui sert à lancer le système d'exploitation
``/dev``
   accès aux périphériques
``/etc``
   les fichiers de configuration
``/home``
   les dossiers personnels de chaque utilisateur
``/media``
   contient les points de montages amovibles (clés USB, CDs, etc.)
``/tmp``
   les fichiers temporaires (dossier vidé à chaque démarrage)
``/usr``
   là où s'installe généralement les programmes supplémentaires
``/var``
   contient des fichiers théoriquement effaçables, notamment les *journaux
   d'activité* (ou *logs*), dans le dossier ``/var/log``

Quand on paramètre un programme à l'échelle de l'utilisateur, on a plus de
chances de retrouver ce qui nous intéresse dans le dossier personnel
(``/home/NOM_DE_L_UTILISATEUR`` qui peut s'écrire sous forme raccourcie : ``~``) on y trouve :

``~/.cache``
   les fichiers de *cache* (théoriquement effaçable) par exemple le cache du navigateur
``~/.config``
   la configuration par l'utilisateur
``~/.local/share``
   les données des applications et notamment la corbeille dans le dossier
   ``~/.local/share/Trash/files``

