Utiliser Ubuntu
========================================================

Présentation d'Ubuntu ainsi que quelques pistes pour l'installer, la configurer
et l'utiliser en faisant gaffe à la sécurité.

Le répertoire contient une documentation `Sphinx <https://sphinx-doc.org>`_ à
compiler.
