La ligne de commande
====================

Aussi appellée *terminal*, *console*, *shell*, *bash* ... il s'agit d'une autre
manière d'interagir avec le système que de passer par les applications
graphiques. On va souvent en avoir besoin pour installer des programmes
spécifiques ou résoudre des problèmes. 

Pour résumer on va écrire ce qu'on veut faire plutôt que cliquer. Je préfère
renvoyer à d'autres documents pour plus d'explications :

* `Le terminal GNU/Linux (wiki Ubuntu-fr) <https://doc.ubuntu-fr.org/terminal>`_
  (jusqu'au chapitre 14, après ça se lance dans de la personnalisation sans
  intérêt)
* `Commandes et lignes de commandes <https://doc.ubuntu-fr.org/commande_shell>`_
  (liste des commandes et leur utilité)
* `The Linux command line for beginner <https://ubuntu.com/tutorials/command-line-for-beginners>`_ (plus détaillé
  mais en anglais)

Quelques notes personnelles
---------------------------

* on ne va pas tout apprendre d'un coup, on progresse doucement, astuce après
  astuce, c'est normal
* Les raccourcis de copier-coller :kbd:`Ctrl+C` et :kbd:`Ctrl+V` ne fonctionnent
  pas comme on s'y attendrait. :kbd:`Ctrl+C` est très utile car elle interrompt
  les opérations en cours. Quand on veut copier-coller il faut utiliser
  :kbd:`Ctrl+Maj+C` et :kbd:`Ctrl+Maj+V`, on s'y fait.
* Les commandes précédées de ``sudo`` impliquent de lancer la commande en tant
  que super-utilisateur. Ça peut avoir des conséquences néfastes si c'est mal
  utilisé.
* Quand on a besoin d'utiliser un éditeur de texte en ligne de commande, le plus
  simple c'est d'utiliser ``nano``. Les raccourcis claviers utiles sont indiqués
  en bas de l'écran, ``^X`` signifique :kbd:`Ctrl+X`. Après quelques utilisations
  on s'en sort facilement. C'est surtout utile quand on doit faire une
  modification en tant que super-utilisateur car sinon on peut utiliser
  l'éditeur de texte graphique *gedit*.
* Toutes les commandes sont enregistrées dans un historique. Pour y accéder il y
  a plusieurs moyens :

  * utiliser les flèches :kbd:`↑` et :kbd:`↓`
  * utiliser la commande ``history``
  * faire :kbd:`Ctrl+R` pour rechercher une commande (par exemple en tapant
    ``apt`` car on sait qu'on a utilisé apt + quelque chose).
    
  Pour qu'une commande soit oubliée, il faut la faire précéder d'un espace.
* Ça peut très vite devenir très compliqué, il faut parfois réfléchir avant de
  lancer une commande qu'on ne comprend pas. Une bonne idée c'est de savoir ce
  qu'on peut faire pour inverser la commande.
