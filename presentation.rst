Présentation générale d'Ubuntu, de Debian, de GNU/Linux ...
===========================================================

Voici une liste de termes qui s'appliquent plus ou moins bien pour donner une
définition d'Ubuntu : de façon très vague on pourrait dire que c'est un *système
d'exploitation* (en anglais : *operating system* abrégé *OS*, abréviation que
l'on voit parfois en français). On pourrait être plus spécifique en disant que
c'est une *distribution Linux* qu'on pourrait aussi appeler *distribution
GNU/Linux*.

Un système d'exploitation
-------------------------

Ce qu'est exactement un *système d'exploitation*, on s'en fout un peu, on peu
en citer quelques uns : *Windows* de Microsoft, *Mac OS* et *iOS* d'Apple,
*Android* de Google, et pléthore de distributions Linux dont on peut citer
*Debian* ou *Tails*.

Certains de ces sytèmes d'exploitation sont fondés sur le système *Unix* qui
est une sorte de base qui existe depuis les années 1970, qui détermine un peu
la manière de concevoir le système et qui a donné quelques normes.

Ça a pour conséquence que les manières de procéder sous Windows ont peu de
chances d'être les mêmes sur Ubuntu. On aura plus de similarités avec Mac OS,
ou encore Android. On peut donc parfois trouver des réponses à un problèmes en
cherchant des solutions concernant les systèmes Unix.

Une distribution Linux
----------------------

Contrairement à Windows qui ne fait qu'évoluer dans le temps avec des versions,
dans l'univers linuxien on trouve plein de variantes appellées *distributions
Linux* (parfois appellées *distro*). Chacune correspond à des choix
d'organisation entre développeurs, d'objectifs et de résultats concrèts.

Debian et Tails sont des distributions non-commerciales gérées (paraît-il)
collaborativement. Ubuntu est une distribution commerciale gérée par
l'entreprise Canonical. 

Debian semble plutôt être axée sur la stabilité, la sécurité. Tails est conçue
pour des utilisations spécifiques, afin de laisser pas ou peu de traces de son
activité que ça soit sur internet ou hors-connexion. Ubuntu est prévue pour le
grand public, a en partie réussi son objectif et a une assez grosse communauté.

Certaines distributions Linux servent de bases à d'autres. Debian sert de base à
de nombreuses distribution dont justement Tails et Ubuntu. Ça signifie qu'un
programme prévu pour Debian a de grandes chances de fonctionner sur Ubuntu sans
trop d'effort.

Quand on a une question à résoudre, on a donc beaucoup de chances de trouver la
réponse en cherchant des solutions pour Ubuntu, sinon pour Debian, autrement on
a encore pléthore de distributions parmis lesquelles on peut trouver des
informations, personnellement j'utilise parfois `le wiki d'Arch Linux
<https://wiki.archlinux.org>`_. On peut aussi faire des recherches en utilisant
Linux comme mot-clé, tout va dépendre du problème que l'on rencontre.

Le noyau Linux
--------------

Si on parle parfois de *GNU/Linux* c'est parce qu'en réalité, le terme Linux est
censé designer ce qu'on appelle *le noyau Linux* (ou *kernel* en anglais). Pour
essayer de comprendre ce qu'est le noyau, il faut voir l'ensemble : au plus *bas
niveau* on a le matériel informatique qui travaille en binaire (on peut voir ça
comme des 0 et des 1), au plus *haut niveau* on a une application graphique
qu'on peut utiliser avec une souris par exemple. Le noyau se situe au plus près
du bas niveau, pour permettre à des logiciels de fonctionner sans connaître les
détails de chaque type d'écran ou de souris existantes par exemple. Seul il est
quasiment inutilisable, il lui faut encore de nombreux programmes pour en faire
un système d'exploitation. Il est utilisé par Android pour les téléphones et
un ensemble plus variable pour les PC (les distributions Linux).

.. important:: dans les systèmes d'exploitation que sont les distributions Linux on
   a donc une superposition de logiciels pour passer d'un noyau *bas niveau* à
   des applications directement utilisables comme un navigateur web. Ça me
   paraît impossible de tout lister, je me contente de faire un petit tour de
   celles où on est parfois amenés à bricoler un peu.

L'environnement de bureau
-------------------------

Les distributions fournissent aux applications des moyens de créer des fenêtres,
des boutons, etc. avec un style similaire, on va simplifier en appelant ça
*l'environnement de bureau*, *environnement graphique* ou *interface graphique*.
Ubuntu incorpore un environnement appellé *GNOME*, si on cherche à résoudre des
difficultés d'affichage il est possible qu'on trouve la réponse relativement à
GNOME plus qu'à Ubuntu.

Il existe tout un tas d'autres environnements, *KDE*, *Xfce*, *LXDE*, pas la
peine de les retenir mais Ubuntu propose des variantes appellée Kubuntu,
Xubuntu, Lubuntu avec ces différents environnements, on peut parfois trouver des
réponses sur des sujets traitant de ces variantes.

Les environnements graphiques fournisent généralement un ensemble de programmes,
comme le gestionnaire de fichiers, etc. Si on cherche à installer un programme
prévu pour KDE sur GNOME, on va parfois se retrouver à installer toute une
partie de KDE et un programme qui fait quelques mégaoctets aura besoin de
plusieurs centaines de mégaoctets.

Le serveur X
------------

Il y a en fait une sorte de sous-couche de ces environnements qui s'appelle le
*serveur X* (ou Xorg ou X11, ça dépend et c'est peut-être pas tout à fait la
même chose). On peut en avoir besoin dans certains cas assez spécifique de
problèmes avec le clavier, la souris ou l'écran. Dans mon expérience c'est
jamais simple à comprendre et à utiliser.

systemd
-------

Il y a des programmes qui tournent en fond (on appelle ça parfois les *démons*
ou *deamons*) comme *NetworkManager* qui gère les connexions internet ou des
programmes qu'on a besoin de lancer au démarrage, à la mise en veille, etc. À
l'heure actuelle c'est géré par un truc qui s'appelle systemd. Personnellement
j'ai du mal à comprendre ce que c'est exactement mais on est parfois amené à
farfouiller dedans pour résoudre certains problèmes. Dans ce cas on utilise
souvent la commande ``systemctl``.

En résumé
---------

Si on cherche à résoudre quelque chose on ira :

1. dans la documentation, les forums du programme qui pose problème, si on
   arrive à l'identifier. Dans le cas des programmes fournis par défaut avec
   GNOME, ça peut être délicat. Par exemple, si on prend le gestionnaire de
   fichiers et qu'on cherche dans le menu *À propos*, on nous dit que le
   programme s'appelle *Fichiers* alors qu'en réalité il s'appelle *Nautilus*
   ...
2. à propos d'Ubuntu :

   1. `la documentation francophone d'Ubuntu <https://doc.ubuntu-fr.org>`_ mais
      le contenu est assez inégal
   2. `le forum francophone d'Ubuntu <https://forum.ubuntu-fr.org>`_ en prenant
      garde aux dates des sujets
   3. `la documentation anglophone d'Ubuntu <https://wiki.ubuntu.com>`_
   4. divers subreddits de reddit.com : `r/Ubuntu
      <https://www.reddit.com/r/Ubuntu/>`_, `r/linux
      <https://www.reddit.com/r/linux/>`_, etc.
   5. les communautés StackExchange : `AskUbuntu <https://askubuntu.com/>`_ ou
      éventuellement `Unix & Linux <https://unix.stackexchange.com>`_
3. on peut aussi élargir aux variantes d'Ubuntu
4. dans `la documentation de Debian <https://wiki.debian.org/fr/FrontPage?action=show&redirect=PageD%27Accueil>`_
5. dans `la documentation d'Arch Linux <https://wiki.archlinux.org>`_ citée précédemment
6. dans tout ce qui traite de Linux voire d'Unix

.. note:: personnellement je trouve généralement les solutions traitant de GNOME, du serveur X ou de systemd dans les sources précédentes.

Il existe bien entendu pléthore de sites moins populaires ou de pages
personnelles qui auront de bonnes infos. `Le wiki de Sebsauvage
<https://sebsauvage.net/wiki/doku.php>`_ contient parfois des infos utile. Il
est toujours difficile de faire le tri entre les différentes manières de faires,
il existe beaucoup de vieilles pratiques pas très utiles. Malheureusement, seule
l'expérience permet de faire le tri.
